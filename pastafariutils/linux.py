#!/usr/bin/epython3

import os
import distro
from subprocess import call, DEVNULL
import re
import json

def shell_command(command):
    
    if call(command, shell=True) > 0:
        print('Error: cannot execute command')
        return False

def check_distro(arr_command):
    
    distro_id=distro.id()
    
    if not distro_id in arr_command:
        
        print("Sorry, you don't have a patch for this distro\n\n")
        
        return False
    else:
        return distro_id

def install_package(package):
    
    distro_id=distro.id()
    
    if distro_id=='debian' or distro_id=='ubuntu':
        
        return shell_command('sudo DEBIAN_FRONTEND="noninteractive" apt-get install -y {}'.format(package[distro]))
        
    elif distro_id=='fedora' or distro_id=='almalinux' or distro_id=='rocky':
        
        return shell_command('sudo dnf install -y {}'.format(package[distro]))

    elif distro_id=='arch':
        
        return shell_command('sudo pacman -S --noconfirm {}'.format(package[distro]))
    
    
def patch_file(original_file, patch_file):
    
    distro_id=check_distro(original_file)
        
    if not distro_id:
        
        return False
    
    return shell_command("sudo patch {} < {}".format(original_file[distro], patch_file[distro]))
    

def systemd_service(action, service):
    
    distro_id=check_distro(service)
        
    if not distro_id:
        
        return False

    return shell_command('sudo systemctl {} {}'.format(action, service[distro]))
    

def exec(command):
    
    distro_id=check_distro(command)
        
    if not distro_id:
        
        return False

    return shell_command(command[distro])


def sed(arr_sed):
    
    distro_id=check_distro(arr_sed)
        
    if not distro_id:
        
        return False

    return shell_command("sudo sed -i s/{}/{}/g".format(arr_sed[distro][0], arr_sed[distro][1]))

    
def json_log(message, error=0, status=0, progress=0, no_progress=0, return_message=0):
    
    log={'error': error, 'status': status, 'progress': progress, 'no_progress': no_progress, 'message': message}
    
    if not return_message:
    
        print(json.dumps(log))
        
    else:
        return json.dumps(log)
    
