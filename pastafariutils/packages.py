#!/usr/bin/env python3

import os
import distro
from subprocess import call, DEVNULL
import re

def install_package(package: dict, extra_configurations={}):

    """A function for install packages for different distros. Now support, debian, ubuntu, rocky, almalinux, fedora, archlinux. 

    Args:
        package (dict): A dict with all packages names for every distro.

    Returns:
    
        result_package_manager (bool): return false if install fail, if install is finished, return true. 
    """

    linux_distro=distro.id()
    
    if not linux_distro in package:
        print('Sorry, not package in {}'.format(linux_distro))
        return False
    
    if linux_distro=='debian' or linux_distro=='ubuntu':
        
        if call('sudo DEBIAN_FRONTEND="noninteractive" apt-get install -y {}'.format(package[linux_distro]), shell=True) > 0:
            print('Error, cannot install {}...'.format(package[linux_distro]))
            return False
            
    elif linux_distro=='rocky' or linux_distro=='fedora' or linux_distro=='almalinux':
        
        if call("sudo dnf install -y {}".format(package[linux_distro]), shell=True) > 0:
            print('Error, cannot install {}...'.format(package[linux_distro]))
            return False
    if linux_distro=='arch':

        if call("sudo pacman -S --noconfirm {}".format(package[linux_distro]), shell=True) > 0:
            print('Error, cannot install {}...'.format(package[linux_distro]))
            return False      
    
# Method for patch files using patch utility

def apply_patch(original_file: dict, patch_file: dict):
    
    linux_distro=distro.id()
    
    # patch originalAmigo.sh < parche.patch
    
    if not linux_distro in original_file or not linux_distro in patch_file):
        print('Error, not exists original file and patch files for this distro'))
        return False      

    if call("sudo patch {} < {}".format(original_file[linux_distro], patch_file[linux_distro]), shell=True) > 0:
        print('Error, cannot patch {}...'.format(original_file[linux_distro]))
        return False      
# A simple function for fill an array for every distros with the same package name. 

def fill_all_distros_str(name):
    
    fill_str={}
    
    for distro in ['debian', 'ubuntu', 'rocky', 'almalinux', 'fedora', 'arch']:
        fill_str[distro]=name
        
    return fill_str
        
# A simple function for get a json return value        

def return_json_value():
    
    return {}
