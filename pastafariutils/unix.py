#!/usr/bin/env python3

import time
import os
import re
import argparse
import json
import pwd
import sys
import crypt
from subprocess import call, DEVNULL

def add_user(user, password='', group='', user_directory='', shell='/usr/sbin/nologin'):
    
    if user_directory=='':
        user_directory='/home/'+user
    
    try:
        user_pwd=pwd.getpwnam(user)

        return (False, 'User exists')

    except KeyError:
 
        # add user

        if password!='':
        
            salt=crypt.mksalt(crypt.METHOD_SHA512)
            
            password='-p \"%s\"' % crypt.crypt(password, salt).replace('$', '\$')

        if group!='':
            
            # Buggy, need fix. 
            
            stat_group=os.stat('/home/%s' % user_directory)
            gid=stat_group.st_gid

            func_user="sudo useradd -m -s %s -g %i %s -d %s %s" % (shell, gid, password, user_directory, user)
            
        else:

            func_user="sudo useradd -m -s %s %s -d %s %s" % (shell, password, user_directory, user)            

        if call(func_user,  shell=True, stdout=DEVNULL) > 0:
            
            return (False, 'Error executing useradd command')
            
        else:
            
            return (True, '')
            
            

def change_password(user, new_password):
    
    try:
        user_pwd=pwd.getpwnam(user)
        
        if call("sudo echo \"%s:%s\" | sudo chpasswd" % (user, new_password),  shell=True, stdout=DEVNULL) > 0:
            
            return (False, 'I cannot change password, permissions?')
            
        else:
            return (True, 'Change password successfully')
            

    except KeyError:
    
        return (False, 'I cannot change password, user exists?')
    
def del_user(user):

    if call("sudo userdel -r %s" % user,  shell=True, stdout=DEVNULL, stderr=DEVNULL) > 0:
        return (False, '')
    else:
        return (True, 'Deleted user successfully')

def mkdir_sh(directory):
    
    if call("sudo mkdir -p %s" % directory,  shell=True) > 0:
        return (False, '')
    else:
        return (True, 'Created directory successfully %s' % directory)
    
