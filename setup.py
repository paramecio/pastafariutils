#!/usr/bin/env python3

import sys
import os
from setuptools import setup, find_packages


if sys.version_info < (3, 6):
    raise NotImplementedError("Sorry, you need at least Python 3.6 for use pastafariutils.")

#import paramecio
# Pillow should be installed after if you need ImageField
# If you install passlib and bcrypt, the password system will use bcrypt by default, if not, will use native crypt libc 

setup(name='pastafariutils',
      version='0.6.3',
      description='Utils for make *nix scripts.',
      long_description='Utils for make *nix scripts.',
      author='Antonio de la Rosa Caballero',
      author_email='antonio.delarosa@coesinfo.com',
      url='https://bitbucker.org/paramecio/parameciofm/',
      packages=['pastafariutils'],
      include_package_data=True,
      install_requires=[],
      license='GPLV3',
      platforms = 'any',
      classifiers=['Development Status :: 1 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GPLV2 License',
        'Topic :: Software Development :: Libraries :: Utils for *nix servers',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7'
        ],
     )
